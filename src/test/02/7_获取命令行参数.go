package main

import (
	"os"
	"fmt"
)

func main()  {
	args := os.Args

	if args == nil || len(args) <=2 {
		fmt.Println("err: xxx ip port")
		return
	}

	ip := args[1]
	port := args[2]
	fmt.Printf("ip = %s, port = %s\n", ip, port)

	//go run 7_获取命令行参数.go 192.168.0.1 8080
	//ip = 192.168.0.1, port = 8080
}
