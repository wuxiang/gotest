package main

import (
	f "fmt"  // 别名操作
	_ "os"  //_操作其实是引入该包，而不直接使用包里面的函数
)

func main()  {
	f.Println("2222")
}

func init()  {
	f.Println("1111")
}
