package main

import "fmt"
//如果一个函数中有多个defer语句，它们会以LIFO（后进先出）的顺序执行

func main()  {
	fmt.Println("defer……")
	defer fmt.Println("this is a defer")

	defer fmt.Println("aaaaaaaa")
	defer fmt.Println("bbbbbbbb")

	defer test(12)

	defer fmt.Println("cccccccc")
}

func test(x int) {
	fmt.Println(100 / x)//x为0时，产生异常
}
