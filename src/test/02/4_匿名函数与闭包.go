package main

import "fmt"

func main()  {
	f1 := func(){
		fmt.Println("这是一个匿名函数")
	}
	f1()

	func(){
		fmt.Println("这也是一个匿名函数")
	}()

	v := func(a, b int) (result int) {
		result = a + b
		return
	}(1, 1) //别忘了后面的(1, 1), (1, 1)的作用是，此处直接调用此匿名函数， 并传参
	fmt.Println("v = ", v)

}
