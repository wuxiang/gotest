package main

import "fmt"

type Test func(int,int)int  //声明一个函数类型, func后面没有函数名

func Test2(a,b int,f Test)(result int)  {  //函数中有一个参数类型为函数类型：f FuncType
	result = f(a,b)  //通过调用f()实现任务
	return result
}
func Add(a,b int) int  {
	return  a+b
}

func main()  {
	//函数调用，第三个参数为函数名字，此函数的参数，返回值必须和FuncType类型一致
	result := Test2(1, 1, Add)
	fmt.Println(result) //2

}
