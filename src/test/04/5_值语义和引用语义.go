package main

import "fmt"

type Person struct {
	id int
	name string
	sex byte
}

//指针引用
func (p *Person)SetPointer()  {
	(*p).id = 1
	(*p).name = "张三"
	(*p).sex = 'm'
	fmt.Println(p)
}

func (p Person) setInfo()  {
	p.name = "yoyo"
	p.sex = 'f'
	p.id = 22

}
func main()  {
	p1:=Person{2,"李四",'s'}
	fmt.Println(p1)
	(&p1).SetPointer()
	fmt.Println(p1)

	p2:=Person{2,"李四",'s'}
	fmt.Println(p2)
	p2.setInfo()
	fmt.Println(p2)
}
