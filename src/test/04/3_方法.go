package main

import "fmt"

type MyInt int

func (t MyInt)Add(a MyInt) MyInt  {
	return a + t
}
func main()  {

	var a MyInt = 1
	var b MyInt = 1

	//调用func (a MyInt) Add(b MyInt)
	fmt.Println("a.Add(b) = ", a.Add(b)) //a.Add(b) =  2


}
