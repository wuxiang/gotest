package main

import "fmt"

type Student struct {
	id int
	name string
}

func (p Student)PrintInfo()  {
	fmt.Println(p.id,p.name)
}

func main()  {
	p := Student{1,"张三"}
	p.PrintInfo();
}
