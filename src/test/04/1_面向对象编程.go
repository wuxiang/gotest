package main

import "fmt"

//人
type Person struct {
	id int
	name string
	sex byte
	age int
} 

//学生
type Student struct {
	Person //继承person
	id int
	addr string
}

type Man struct {
	Person
	int
	string
}

func main()  {
	s := Student{Person{5,"张三",'m',18},1,"白马"}
	s2 := Student{Person{name:"张三"},1,"白马"}
	fmt.Println(s)
	fmt.Println(s2)

	var s3 Student
	s3.addr = "溧水"
	s3.age = 20
	s3.Person.id = 2
	s3.id = 4
	fmt.Println(s3)

	var s4 Man
	s4.int = 2
	fmt.Println(s4)
}
