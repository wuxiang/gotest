package main

import "fmt"

type Person struct {
	id int
	name string
	age int
}

type Student struct {
	*Person
	addr string
}

func main()  {
	s := Student{&Person{1,"巡航",12},"111"}
	fmt.Printf("%t",s)
}
