package main

import "fmt"

//_（下划线）是个特殊的变量名，任何赋予它的值都会被丢弃：
func main()  {
	_,str := test()
	fmt.Println(str)
}

func test()(int,string){
	return 250,"sb"
}
