package main

import "fmt"

func main()  {
	a := 10
	switch a {
	case 90:
		fmt.Println("优秀")
	case 10:
		fmt.Println("桂花糊")

	}

	var s2 int = 90
	switch { //这里没有写条件
	case s2 >= 90: //这里写判断语句
		fmt.Println("优秀")
	case s2 >= 80:
		fmt.Println("良好")
	default:
		fmt.Println("一般")
	}

}
