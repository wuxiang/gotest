package main

import "fmt"

//在一个const声明语句中，在第一个声明的常量所在的行，iota将会被置为0，然后在每一个有常量声明的行加一。
func main(){
	const (
		a = iota
		b = iota
		c = iota
		d
		e
	)

	fmt.Println(a,b,c,d,e)

	const (
		x1 = iota * 10 // x1 == 0
		y1 = iota * 10 // y1 == 10
		z1 = iota * 10 // z1 == 20
	)
	fmt.Println(x1,y1,z1)
}
