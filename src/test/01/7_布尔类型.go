package main

import "fmt"

func main(){
	//布尔
	a := (1==2)
	fmt.Println(a)
	//整形
	v := 123
	fmt.Printf("%T",v)
	fmt.Println("\n")
	//浮点
	f:=12.00
	fmt.Printf("%T",f)
	fmt.Println("\n")
	//字符
	b := 'b'
	fmt.Println(b)
	fmt.Printf("%T",b)
	fmt.Printf("%c",b)

}
