package main

import "fmt"

func main()  {
	var arr[5] int
	fmt.Println(arr)
	fmt.Println(len(arr))
	arr[1] = 3
	arr[2] = 4
	fmt.Println(arr)

	for i,v := range arr {
		fmt.Printf("%d,%d\n",i,v)
	}

	//初始化
	arrs := [5]int{1,2}
	fmt.Println(arrs)
	arrs = [5]int{3:10,4:20}
	fmt.Println(arrs)
}
