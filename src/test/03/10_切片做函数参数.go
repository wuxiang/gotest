package main

import "fmt"

func main()  {
	arr := [10]int{0,1,2,3,4,5,6,7,8,9}
	sarr := arr[2:]
	aslice(sarr)
	fmt.Println(sarr)
}

func aslice(a []int) {
	for i,v := range a{
		fmt.Printf("slice[%d]=%d, ", i, v)
	}
}
