package main

import "fmt"

type Student struct {
	id int
	name string
	sex byte
	age int
}
func main()  {

	//1、顺序初始化，必须每个成员都初始化
	var s Student = Student{1,"张三",'m',18}
	fmt.Println(s)

	s2:=Student{2,"张三",'m',18}
	fmt.Println(s2)

	//指定初始化某个成员，没有初始化的成员为零值
	s3 := Student{name:"张三",age:20}
	fmt.Println(s3)

	s6 := &Student{4,"milk",'m',25}
	fmt.Println(s6)

	//赋值
	var s7 Student
	s7.id = 6
	s7.name = "李四"
	fmt.Println(s7)

	//------------------------------
	s8 := new(Student)
	s8.id = 7
	s8.name = "王五"
	fmt.Println(s8)

	//-------------
	var p *Student = &s7
	p.id = 8
	fmt.Println(p)
}