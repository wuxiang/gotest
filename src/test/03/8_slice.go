package main

import "fmt"

func main()  {
	arr := [...]int{1,2,3}
	fmt.Println(len(arr),cap(arr))

	//声明切片和声明array一样，只是少了长度，此为空(nil)切片
	arr2 := []int{}
	fmt.Println(arr2)

	arr3:=[]int{1,2,3}
	fmt.Println(arr3)

	arr4 := make([]int,0,0)
	fmt.Println(arr4)

	array := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	array2 := array[:3] //从切片s的索引位置0到high处所获得的切片，len=high
	fmt.Println(array,array2)
	fmt.Println(array[3:5]) //从切片s的索引位置low到high处所获得的切片，len=high-low
	fmt.Println(array[3:])//从切片s的索引位置low到len(s)-1处所获得的切片
}
