package main

import "fmt"

func main()  {
	a,b := 10,20
	swap(&a,&b)
	fmt.Println(a,b)
}

func swap(a,b *int)  {
	*a,*b = *b,*a
	fmt.Println(*a,*b)
}
