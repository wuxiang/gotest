package main

import "fmt"

func main()  {
	var a int
	var p *int

	p = &a

	fmt.Println(p)
	*p = 666

	fmt.Println(a)
}
