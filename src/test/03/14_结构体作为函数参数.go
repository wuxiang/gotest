package main

import "fmt"

type Person struct {
	id int
	name string
	age int
	sex byte
}
func main()  {
	a := Person{1,"张三",12,'m'}
	//值传递
	printPersonValue(a)
	fmt.Println(a)
	//引用传递
	printPersonPoint(&a)
	fmt.Println(a)
}

func printPersonValue(a Person)  {
	a.id = 2
	fmt.Println(a)
}

func printPersonPoint(a *Person)  {
	a.id = 3
	fmt.Println(a)
}
