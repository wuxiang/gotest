package main

import (
	"fmt"
	"errors"
)

func main()  {
	err1 := fmt.Errorf("%s","this is a normal error")
	fmt.Println(err1)

	err2 := errors.New("this is errors")
	fmt.Println(err2)
}
