package main

import (
	"encoding/json"
	"fmt"
)

type IT struct {
Company  string
Subjects []string
IsOk     bool
Price    float64
}

func main() {

	t1 := make(map[string]interface{})
	t1["company"] = "itcast"
	t1["subjects "] = []string{"Go", "C++", "Python", "Test"}
	t1["isok"] = true
	t1["price"] = 666.666

	b, err := json.Marshal(t1)
	if err != nil {
		fmt.Println("json err:", err)
	}
	fmt.Println(string(b))

	//fmt.Println(json.Unmarshal(b))
}

