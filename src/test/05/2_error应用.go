package main

import (
	"errors"
	"fmt"
)

func test(a,b int) (result int,err error) {
	if b==0 {
		err = errors.New("除数不能为空")
	}else{
		result  = a / b
	}
	return
}
func main()  {
	res,err := test(1,5)
	if err==nil{
		fmt.Println(res)
	}else{
		fmt.Println(err)
	}

}
