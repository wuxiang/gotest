package main

import (
	"strings"
	"fmt"
)

func main()  {
	str := "abcdefg"

	if strings.Contains(str,"a"){
		fmt.Println(true)
	}else{
		fmt.Println(false)
	}

	str2:=[]string{"a","b","c"}
	fmt.Println(strings.Join(str2,","))

	str3 := "hello world"
	fmt.Println(strings.Index(str3,"ld"))

	fmt.Println("ba" + strings.Repeat("na", 2))


}
