package main

import (
	"runtime"
	"fmt"
)

func main()  {
	n:=runtime.GOMAXPROCS(1)
	fmt.Println("n=%d\n",n)

	for{
		go fmt.Print(0)
		fmt.Print(1)
	}
}
