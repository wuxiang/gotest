package main

import (
	"fmt"
)

func main()  {
	c := make(chan int)
	//fmt.Printf("len(c)=%d,cap(c)=%d\n",len(c),cap(c))

	go func() {
		//defer fmt.Println("子协程结束")
		for i:=0;i<10 ;i++  {
			c<-i
			//fmt.Printf("子协程正在运行[%d]:len(c)=%d,cap(c)=%d\n",i,len(c),cap(c))
		}
		//close(c)
	}()
	
	//time.Sleep(2 * time.Second)

	//for i:=0;i<3 ;i++  {
	//	num := <- c
	//	fmt.Println("num=",num)
	//}
	//fmt.Println("main协程结束")

	for  {
		if data,ok:=<-c;ok {
			fmt.Println(data)
		}else {
			break
		}
		
	}
	fmt.Println("finised")
}
