package main

import (
	"fmt"
	"time"
)

func main()  {
	go newTask()

	fmt.Println("main goroutine exit")
	//i :=0
	//
	//for {
	//	i++
	//	fmt.Println("new goroutine:i=%d\n",i)
	//	time.Sleep(1 * time.Second)
	//}
}

func newTask(){
	i :=0
	for {
		i++
		fmt.Println("main goroutine:i=%d\n",i)
		time.Sleep(1 * time.Second)
	}
}
