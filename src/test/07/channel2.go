package main

import (
	"fmt"
	"time"
)

func main()  {
	c:=make(chan int,0)

	fmt.Printf("len(c)=%d,cap(c)=%d\n",len(c),cap(c))

	go func() {
		defer fmt.Println("子协程结束")

		for i:=0;i<3 ;i++  {
			c<-i
			fmt.Printf("子协程正在运行【%d】:len(c)=%d,cap(c)=%d\n",i,len(c),cap(c))
		}
	}()

	time.Sleep(2 * time.Second)

	for i:=0;i<3 ;i++  {
		n := <-c
		fmt.Println("n=",n)
	}

	fmt.Println("main协程结束")
}
