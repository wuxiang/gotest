package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
)

func main()  {
	res,err:=http.Get("http://www.baidu.com")
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	data,err := ioutil.ReadAll(res.Body)
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	fmt.Println(string(data))
}
