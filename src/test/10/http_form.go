package main

import (
	"net/http"
	"io"
	"fmt"
	"log"
)

const form="<html><body><form action='#' method='post'>" +
	"<input type='text' name='in'><input type='text' name='t'><input type='submit' name='submit'></form></body></html>"

func Test(w http.ResponseWriter,r *http.Request)  {
	io.WriteString(w,"hello world")
}

func Test2(w http.ResponseWriter,r *http.Request){
	w.Header().Set("Content-Type","text/html")
	switch r.Method{
	case  "GET":
		io.WriteString(w,form)
	case "POST":
		r.ParseForm()
		io.WriteString(w,r.FormValue("in"))
		io.WriteString(w,r.FormValue("t"))

	}
}

func logPanic(handle http.HandlerFunc)http.HandlerFunc  {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer func() {
			if x:=recover();x!=nil{
				log.Printf("[%v] caught panic : %v,",request.RemoteAddr,x)
			}
		}()
		handle(writer,request)
	}
}

func main()  {
	http.HandleFunc("/test1",logPanic(Test))
	http.HandleFunc("/test2",logPanic(Test2))
	err := http.ListenAndServe("0.0.0.0:8080",nil)
	if err!=nil{
		fmt.Println("err:",err)
	}
}
