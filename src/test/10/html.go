package main

import (
	"html/template"
	"fmt"
	"os"
)

type Person struct {
	Name string
	age int
}
func main()  {
	t,err := template.ParseFiles("./index.html")
	if err != nil{
		fmt.Println("err:",err)
		return
	}
 	stu := Person{"张三",18}
	if err:=t.Execute(os.Stdout,stu);err!=nil{
		fmt.Println("err:",err.Error())
	}
}
