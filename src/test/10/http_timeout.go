package main

import (
	"net/http"
	"fmt"
	"net"
	"time"
)

func main()  {
	url := "http://www.baidu.com"
	c:=http.Client{
		Transport:&http.Transport{
			Dial: func(network, addr string) (net.Conn, error) {
				timeout:=time.Second * 30
				return net.DialTimeout(network, addr,timeout)
			},
		},
	}
	res,err:=c.Get(url)
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	fmt.Println(res.Status)
}
