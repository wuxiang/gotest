package main

import (
	"fmt"
	"net/http"
	"html/template"
)

type Person2 struct {
	Name string
	Age int
}

func UserInfo(w http.ResponseWriter, r *http.Request) {
	myTemp,err := template.ParseFiles("./index.html")
	if err != nil{
		fmt.Println("err:",err)
		return
	}
	p := Person2{"李四",25}
	myTemp.Execute(w,p)
	//myTemp.Execute(os.Stdout,p)

	//file,err :=os.OpenFile("d:/test.log",os.O_CREATE|os.O_WRONLY,0775)
	//if err!=nil{
	//	fmt.Println("err:",err)
	//	return
	//}
	//myTemp.Execute(file,p)
}


func main() {
	http.HandleFunc("/user/info", UserInfo)
	err := http.ListenAndServe("0.0.0.0:8080", nil)
	if err != nil {
		fmt.Println("http listen failed")
	}
}