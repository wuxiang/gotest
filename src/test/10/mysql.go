package main

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"fmt"
)

type Person struct {
	UserId int `db:"user_id"`
	UserName int `db:"user_name"`
	Sex string `db:"sex"`
	Email string `db:"email"`
}

type Place struct {
	Country string `db:"country"`
	City string `db:"city"`
	TelCode string `db:"telcode"`
}

var Db *sql.DB

func init()  {
	database,err := sql.Open("mysql","root:@tcp(localhost	:3306)/test")
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	Db = database
}
func main()  {
	r,err :=Db.Exec("insert into person (username,sex,email) value (?,?,?)","stu001","n","111@qq.com")
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	id,err:=r.LastInsertId()
	if err!=nil{
		fmt.Println("err:",err)
		return
	}
	fmt.Println(id)
}
