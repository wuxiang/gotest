package main

import "fmt"

func main()  {
	c := make(chan int,10)

	for i:=0 ; i<10 ;i++{
		c <- i
	}
	close(c)

	//for{
	//	v,ok := <-c
	//	if ok==false {
	//		break
	//	}
	//	fmt.Println(v)
	//}

	for v := range c{
		fmt.Println(v)
	}

}
