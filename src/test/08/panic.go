package main

import "fmt"

func test()  {
	defer func() {
		if err := recover();err!=nil{
			fmt.Println("panic:",err)
		}
	}()
	var a map[string] int
	a["stu"] = 100
}
func main()  {
	for i:=0;i<100 ;i++  {
		go test()
	}
}

