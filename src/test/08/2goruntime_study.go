package main

import (
	"fmt"
)

type Tast struct {
	n int
}

func calc(num *Tast,c chan int)  {

	sum := 1

	for i:=1;i < num.n ;i++  {
		sum *= i
	}

	c <- sum
}
func main()  {
	//c := make(chan  int)
    var c chan int
	for i:=1;i<10 ;i++  {
		t := &Tast{i}
		go calc(t,c)
	}

	for k:= range c {
		fmt.Println(k)
	}

	for{

	}
}
