package main

import "fmt"

func send(ch chan int,exit chan bool)  {
	for i:=0;i<10 ;i++  {
		ch <- i
	}
	exit <- true
	close(ch)
}

func revice(ch <-chan int,exit chan bool){
	for  {
		v,ok := <-ch
		if ok==false {
			break
		}
		fmt.Println(v)
	}
	exit <- true

}
func main()  {

	ch := make(chan  int,10)
	exitChat := make(chan bool,2)
	go send(ch,exitChat)
	go revice(ch,exitChat)

	for i:=0; i<2;i++  {
		<-exitChat
	}
	close(exitChat)
}
