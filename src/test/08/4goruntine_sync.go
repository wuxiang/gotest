package main

import "fmt"

func writeInt(res,c chan int,exitChan chan bool)  {
	for v := range res{
		flag := true
		for i :=2 ; i <v ;i++ {
			if v % 2 ==0 {
				flag = false
				break
			}
		}
		if flag==true {
			c <- v
		}
	}

	fmt.Println("exit")
	exitChan <- true
}
func main()  {
	intChan := make(chan int,1000)
	resChan := make(chan  int,1000)

	exitChan := make(chan bool,8)

	go func() {
		for i:=1;i<1000 ;i++  {
			intChan <- i
		}

		close(intChan)
	}()

	for i := 0;i<8;i++{
		go writeInt(intChan,resChan,exitChan)
	}

	//等待所有goruntine全部退出
	go func() {
		for i:=0; i < 8; i++  {
			<-exitChan
		}
		close(resChan)
	}()

	for v := range resChan{
		fmt.Println(v)
	}
}
