package main

import (
	"time"
	"fmt"
)

func write(c chan int)  {
	for i:=0;i<20;i++{
		c <- i
	}
}

func read(b chan int)  {
	for{
		a := <- b
		fmt.Println(a)
	}
}
func main()  {
	chanStr := make(chan int,10)
	go write(chanStr)
	go read(chanStr)
	time.Sleep(time.Second * 10)
}
