package main

import (
	"os"
	"fmt"
)

func main(){

	f,err := os.Create("./test.log")
	if err != nil {
		fmt.Println(err)
	}

	defer f.Close()

	for i:=0 ;i<5;i++{
		o := fmt.Sprintf("%s:%d\n","hello world",i)
		f.WriteString(o)
		f.Write([]byte("abcd\n"))
	}
}
