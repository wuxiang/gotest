package main

import (
	"os"
	"fmt"
)

func main()  {
	f,err := os.Open("./test.log")
	if err!=nil{
		fmt.Println(err)
	}
	defer f.Close()

	buf := make([]byte,1024)

	for {
		n,_:=f.Read(buf)
		if n==0 {
			break
		}
		fmt.Println(string((buf)))
	}
}
